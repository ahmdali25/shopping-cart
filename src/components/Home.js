import React, { Component } from 'react';
import { connect } from 'react-redux'
import { addToCart } from './actions/cartActions'

 class Home extends Component{
    
    handleClick = (id)=>{
        this.props.addToCart(id); 
    }

    render(){
        let itemList = this.props.items.map(item=>{
            return(
                <div className="bg-white shadow-lg w-2/5 px-4 py-4" key={item.id}>
                        <div className="flex flex-row gap-x-4">
                            <img className="w-40 bg-gray-100 rounded" src={item.img} alt={item.title}/>
                            <div className="flex flex-col">
                                <span className="text-lg font-semibold">{item.title}</span>
                                <div>
                                    <span className="text-sm font-semibold uppercase">{item.type} {item.color}</span>
                                </div>
                                <div className="mt-4">
                                    <span className="text-sm uppercase">Color {item.color}</span>
                                </div>
                                <div>
                                    <span className="texts-sm uppercase">Size {item.size}</span>
                                </div>
                                <button to="/" className="flex flex-row gap-x-1 mt-4 bg-gray-100 px-1 py-1 shadow-xl rounded" onClick={()=>{this.handleClick(item.id)}}>
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z" />
                                    </svg>
                                    <span className="texts-sm uppercase">Add To Cart</span>
                                </button>
                            </div>
                            <div className="ml-12 mt-32">
                                <p><b>{item.price}$</b></p>
                            </div>
                        </div>
                 </div>

            )
        })

        return(
            <div className="container mx-auto">
                <div className="flex flex-row justify-center mt-10 mb-20">
                    <h3 className="text-2xl font-semibold">E-Shopping</h3>
                </div>
                <div>
                    {itemList}
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state)=>{
    return {
      items: state.items
    }
  }
const mapDispatchToProps= (dispatch)=>{
    return{
        addToCart: (id)=>{dispatch(addToCart(id))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Home)