import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { removeItem,addQuantity,subtractQuantity} from './actions/cartActions'
import Recipe from './Recipe'
class Cart extends Component{

    //to remove the item completely
    handleRemove = (id)=>{
        this.props.removeItem(id);
    }
    //to add the quantity
    handleAddQuantity = (id)=>{
        this.props.addQuantity(id);
    }
    //to substruct from the quantity
    handleSubtractQuantity = (id)=>{
        this.props.subtractQuantity(id);
    }
    render(){
              
        let addedItems = this.props.items.length ?
            (  
                this.props.items.map(item=>{
                    return(
                        <div className="flex flex-col gap-y-4 bg-white px-4 py-4 w-1/2 shadow-lg rounded">
                            <div>
                                <span className="font-semibold">Cart ({item.quantity} Items)</span>
                            </div>
                            <div className="flex flex-row gap-x-4" key={item.id}>
                            <div className="bg-gray-100 rounded"> 
                                <img className="w-40" src={item.img} alt={item.img}/>
                            </div>
                                
                            <div className="">
                                <div>
                                    <span className="text-lg font-semibold">{item.title}</span>
                                </div>
                                <div>
                                    <span className="uppercase">{item.type} {item.color}</span>
                                </div>
                                <div className="mt-4">
                                    <span className="text-sm uppercase">Color {item.color}</span>
                                </div>
                                <div>
                                    <span className="uppercase">Size {item.size}</span>
                                </div>
                                <div className="flex flex-row gap-x-1 mt-5">
                                    <div>
                                        <button className="flex flex-row place-items-center" onClick={()=>{this.handleRemove(item.id)}}>
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
                                                <path fillRule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clipRule="evenodd" />
                                            </svg>
                                            <span className="text-sm uppercase">Remove Item</span>
                                        </button>
                                    </div>
                                    <div>
                                        <button className="flex flex-row place-items-center" onClick={()=>{this.handleRemove(item.id)}}>
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
                                                <path fillRule="evenodd" d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z" clipRule="evenodd" />
                                            </svg>
                                            <span className="text-sm uppercase">Add To Wishlist</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="flex flex-col">
                                <div className="flex flex-row place-items-center gap-x-2 bg-white shadow-lg rounded px-2 py-1 ml-24">
                                    <div className="px-2">
                                        <Link to="/cart">
                                            <i className="font-semibold" onClick={()=>{this.handleSubtractQuantity(item.id)}}>
                                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                                <path fillRule="evenodd" d="M5 10a1 1 0 011-1h8a1 1 0 110 2H6a1 1 0 01-1-1z" clipRule="evenodd" />
                                                </svg>
                                            </i>
                                        </Link>
                                    </div>
                                    <p>{item.quantity}</p>
                                    <div className="px-2">
                                        <Link to="/cart">
                                            <i className="font-semibold" onClick={()=>{this.handleAddQuantity(item.id)}}><svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                                <path fillRule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clipRule="evenodd" />
                                                </svg>
                                            </i>
                                        </Link>
                                    </div>
                                </div>
                                <div className="ml-40 mt-24">
                                    <p className="font-semibold">{item.price}$</p>
                                </div>
                            </div>        
                        </div>
                         
                        </div>
                    )
                })
            ):

             (
                <p>Cart is empty.</p>
             )
       return(
            <div className="container">
                <div className="cart">
                    <ul className="mt-10">
                        {addedItems}
                    </ul>
                </div> 
                <Recipe />          
            </div>
       )
    }
}


const mapStateToProps = (state)=>{
    return{
        items: state.addedItems,
        //addedItems: state.addedItems
    }
}
const mapDispatchToProps = (dispatch)=>{
    return{
        removeItem: (id)=>{dispatch(removeItem(id))},
        addQuantity: (id)=>{dispatch(addQuantity(id))},
        subtractQuantity: (id)=>{dispatch(subtractQuantity(id))}
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Cart)