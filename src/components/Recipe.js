import React, { Component } from 'react'
import { connect } from 'react-redux'
class Recipe extends Component{
    
    render(){
        return(
            <div className="container">
                <div className="flex flex-col w-1/5 bg-white px-4 py-4 rounded shadow-lg mt-4">
                    <div>
                        <h1 className="text-lg font-semibold">The total amount of</h1>
                    </div>
                    <div className="mt-4">
                        <div className="flex flex-row justify-between">
                            <div>
                                <span>Temporary amount:</span>
                            </div>
                            <div>
                                <span>${this.props.total}</span>
                            </div>
                        </div>
                        <div className="flex flex-row justify-between">
                            <div>
                                <span>Shipping</span>
                            </div>
                            <div>
                                <span>Gratis</span>
                            </div>
                        </div>
                    </div>
                    <div className="my-2">
                        <hr className="border-1 border-gray-200"></hr>
                    </div>
                    <div className="flex flex-row justify-between">
                        <div>
                            <span>The total amount of (including VAT)</span>
                        </div>
                        <div>
                            <span>${this.props.total}</span>
                        </div>
                    </div>
                    <div className="flex flex-row justify-center mt-4">
                        <button className="bg-blue-600 rounded px-8 py-2 text-white uppercase">Go To Checkout</button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state)=>{
    return{
        addedItems: state.addedItems,
        total: state.total
    }
}

const mapDispatchToProps = (dispatch)=>{
    return{
        addShipping: ()=>{dispatch({type: 'ADD_SHIPPING'})},
        substractShipping: ()=>{dispatch({type: 'SUB_SHIPPING'})}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Recipe)
